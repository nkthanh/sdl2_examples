#include "Game.h"

Game::Game(){

}

int Game::Execute(){
    if (this->Init() == false){
        LOG("Init Game failure");
        return false;
    }

    if (this->Run() == false){
        LOG("Game throw error when running");
        return false;
    }

    if (this->Exit() == false){
        LOG("Game error when exit");
        return false;
    }

    return 0;
}

bool Game::Init(){
    SDL_Init(SDL_INIT_EVERYTHING);
    Graphic::get();
    LOG("Game init");
    this->isRunning = true;

    return true;
}

bool Game::Run(){
    while (this->isRunning){
        // this->isRunning = false;
        Graphic::get()->Clear();

        Graphic::get()->Update();
        LOG("Game running");
    }

    return true;
}

bool Game::Exit(){
    SDL_Quit();
    LOG("Game exit");

    return true;
}
