#include "Graphic.h"

Graphic* Graphic::instance = NULL;

Graphic* Graphic::get() {
    if (Graphic::instance == NULL) {
        Graphic::instance = new Graphic();
        LOG("Graphic created");
    }
    return Graphic::instance;
}

Graphic::Graphic() {
    this->wind = SDL_CreateWindow(Config::WINDOW_TITLE,
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            Config::SCREEN_WIDTH, Config::SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    this->rend = SDL_CreateRenderer(this->wind, -1,
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    LOG("wind and rend created");
}

Graphic::~Graphic() {
    SDL_DestroyWindow(this->wind);
    SDL_DestroyRenderer(this->rend);
}

void Graphic::Clear() {
    SDL_SetRenderDrawColor(this->rend, 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(this->rend);
}

void Graphic::Update() {
    SDL_RenderPresent(this->rend);
}

