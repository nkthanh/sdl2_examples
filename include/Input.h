#ifndef __INPUT_H__
#define __INPUT_H__

#include <SDL.h>
#include <map>

#include "utils.h"
#include "Config.h"

class Input {
    public:
        ~Input();
        static Input* get();

    private:
        Input();
        static Input* instance;
        SDL_Event* event;
};

#endif
