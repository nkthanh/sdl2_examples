#ifndef __GAME_H__
#define __GAME_H__

#include <SDL.h>

#include "Graphic.h"
#include "utils.h"

class Game {
    public:
        Game();

        int Execute();
    protected:
        bool Init();
        bool Run();
        bool Exit();

    private:
        bool isRunning;
};

#endif
