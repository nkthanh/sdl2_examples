#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#include "Game.h"

int main(int argc, char* args[]){
    Game *my_game = new Game();

    return my_game->Execute();
}

